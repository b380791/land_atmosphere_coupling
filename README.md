# Land Atmosphere Coupling

thoughts on the interface between the atmospheric part of ICON and the underlying land-surface model.
IMPORTANT: while editing the text remember to start each sentence on a new line. This is necessary on git to identify changes to the text easily. Please remember also to add a space after the full stop, even though a new line will start. Otherwise LaTex will glue the sentences directly together. 
